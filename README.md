# Redmine plugin Email Command

A Redmine plugin that provides a command that sends email with Redmine configuration.

## Install

Install this plugin:

```bash
cd redmine
git clone https://gitlab.com/redmine-plugin-email-command/redmine-plugin-email-command.git plugins/email_command
```

Restart Redmine.

## Uninstall

Uninstall this plugin:

```bash
cd redmine
rm -rf plugins/email_command
```

Restart Redmine.

## How to use with systemd

Create a systemd service to send an email by this plugin:

```bash
cd redmine
RAILS_ENV=production plugins/email_command/bin/generate_systemd_service | \
  sudo -H tee /etc/systemd/system/redmine-email@.service
sudo -H systemctl daemon-reload
```

Register this service to `OnFailure`:

```bash
(echo "[Unit]"; echo "OnFailure=redmine-email@%n.service") | \
  sudo -H env EDITOR=tee systemctl edit mysqld
```

## Authors

  * Sutou Kouhei `<kou@clear-code.com>`

  * Shimadzu Corporation

## License

GPLv2 or later. See [LICENSE](LICENSE) for details.

