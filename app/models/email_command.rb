# Copyright (C) 2019  Sutou Kouhei <kou@clear-code.com>
# Copyright (C) 2019  Shimadzu Corporation
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

class EmailCommand < ActionMailer::Base
  layout "mailer"

  def notification
    recipients = params[:to] || []
    from = params[:from]
    if recipients.empty?
      if Setting.respond_to?(:plugin_redmine_exception_handler)
        settings = Setting.plugin_redmine_exception_handler
        recipients = settings["exception_handler_recipients"].split(",").collect(&:strip)
        from ||= settings["exception_handler_sender_address"]
      end
    end
    from ||= Setting.mail_from
    subject = params[:subject] || "[Redmine]"
    @body = params[:body]
    recipients.each do |recipient|
      mail(:to => recipient,
           :from => from,
           :subject => subject,
           "X-Mailer" => "Redmine Email Command",
           "X-Redmine-Host" => Setting.host_name,
           "X-Redmine-Site" => Setting.app_title,
           "X-Auto-Response-Suppress" => "All",
           "Auto-Submitted" => "auto-generated",
           "List-Id" => "<#{from.tr('@', '.')}>")
    end
  end
end
